﻿using System;
using CxServer.Plugins;
using CxServer.Managers;
using CxServer.Commands;

namespace ExamplePluginNamespace
{
	public class ExamplePlugin : Plugin
	{
		public ExamplePlugin() : base("example", 1)
		{
		}

#region implemented abstract members of Plugin

		protected override void Activation()
		{
			
		}

		protected override void PreInit()
		{
			
		}

		protected override void Init()
		{
			var command = CommandManager.RegisterCommand("example");
			command.Execute.Add((ICommandSender sender, Command cmd, string[] args) =>
			{
				sender.SendMessage("Example plugin");
			});
		}

		protected override void PostInit()
		{
			
		}

#endregion
	}
}